/**
 * Created by sasha on 22.9.15.
 */
'use strict';

var app = app || {};

(function($){
  var Room = Backbone.Model.extend({
    defaults: {
      name: '',
      subscribers: []
    },

    url: '/room',

    toJSON: function(){
      var data = Backbone.Model.prototype.toJSON.apply(this);
      data.id = this.id || this.cid;

      return data;
    },

    save: function(){
      if(this.isNew()){
        io.socket.post(this.url, this.toJSON(), $.proxy(this._saved, this));
      }else{
        io.socket.put(this.url, this.toJSON(), $.proxy(this._saved, this));
      }
    },

    _saved: function(data){
      console.log(data);
    }
  });

  var Rooms = Backbone.Collection.extend({
    model: Room,
    url: '/rooms',

    constructor: function(){
      io.socket.get(this.url, $.proxy(function(data){
        console.log(data);
      }, this));

      Backbone.Collection.prototype.constructor.apply(this, arguments);
    }
  });

  app.rooms = new Rooms();


  var Router = Backbone.Router.extend({
    routes: {
      "": "index",
      "room/:id": "room"
    },

    index: function(){
      React.render(
        React.createElement(jsx.indexPages, {rooms: app.rooms}),
        document.querySelector('.container')
      );
    },

    room: function(id){
      console.log('go to room '+id);
    }
  });


  var r = new Router();

  Backbone.history.start({
    pushState: true
  });

})(jQuery);
