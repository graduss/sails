var jsx = jsx || {};

jsx.indexPages = React.createClass({
  getInitialState: function(){
    this.rooms = this.props.rooms;

    this.rooms.on('add',$.proxy(function(room){
      this.setState({
        rooms: this.rooms.toJSON()
      });

      room.save();
    },this));

    return {
      rooms : this.rooms.toJSON()
    }
  },

  onClickNew: function(){
    var room = new this.rooms.model();
    this.rooms.add(room);
  },

  render: function(){
    var elementsOfRoom = this.state.rooms.map($.proxy(function(r,i){
      var room = this.rooms.get(r.id);
      return React.createElement(jsx.roomLine, {room: room, ind: i});
    },this));

    return (
      <div className='row'>
        <div className='col-sx-12'>
          <jsx.rooms parent={this} />
          <table className='table'>
            <thead>
              <tr>
                <th>#</th>
                <th>name</th>
                <th>subscribers</th>
              </tr>
            </thead>
            <tbody>
              {elementsOfRoom}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
});

jsx.rooms = React.createClass({
  render: function(){
    return (
      <div className='form-inline'>
        <div className="form-group">
          <button type='button' className='btn btn-primary' onClick={this.props.parent.onClickNew} >new</button>
          <input type='text' placeholder="room name" className='form-control'/>
          <button type="button" className="btn btn-primary">subscribe</button>
        </div>
      </div>
    )
  }
});

jsx.roomLine = React.createClass({
  getInitialState: function(){
    this.room = this.props.room;

    return this.room.toJSON();
  },

  render: function(){
    return(
      <tr>
        <td>{this.props.ind}</td>
        <td>{this.state.id||this.room.cid}</td>
        <td>{this.state.subscribers.length}</td>
      </tr>
    );
  }
});
