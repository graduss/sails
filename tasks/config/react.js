/**
 * Created by sasha on 6.10.15.
 */

module.exports = function(grunt) {

  grunt.config.set('react', {
    dev : {
      files: [
        {
          expand: true,
          cwd: 'assets/jsx/',
          src: ['*.jsx'],
          dest: '.tmp/public/js/',
          ext: '.js'
        }
      ]
    }
  });

  grunt.loadNpmTasks('grunt-react');
};
