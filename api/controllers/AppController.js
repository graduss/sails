
module.exports = {

  index: function(req, res) {
    return res.view();
  },

  newRoom: function(req, res){
    var uuid = require('node-uuid');
    return res.json({id: uuid.v4()});
  },

  attachRoom: function(req, res){
    if(req.isSocket) {
      var room = req.param('room');
      var id = sails.sockets.id(req.socket);

      sails.sockets.join(req.socket, room);

      return res.json({
        id: id,
        room: room
      });
    }else{
      return res.view('app/index');
    }
  }

};

