/**
 * Created by sasha on 6.10.15.
 */
var uuid = require('node-uuid');

module.exports = {

  index: function(req,res){
    if(req.isSocket){

      var id = sails.sockets.id(req.socket);
      var rooms = sails.sockets.socketRooms(req.socket)
        .filter(function(name){
          return name !== id;
        })
        .map(function(name){
        return {
          name: name,
          subscribers: sails.sockets.subscribers(name)
        };
      });

      return res.json({
        myId: id,
        rooms: rooms
      });

    }else{
      return res.send(404);
    }
  },

  add: function(req,res){
    if(req.isSocket){
      var name = uuid.v4();
      sails.sockets.join(req.socket, name);

      return res.json({
        name: name,
        subscribers: sails.sockets.subscribers(name)
      });
    }else{
      return res.view(404);
    }
  }

};
